﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pplchange
{
    public interface IEntity<T>
    {
        T ID { get; set; }
    }
}
