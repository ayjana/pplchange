﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pplchange.Enums
{
    public enum Category
    {
        Education = 1,
        TrafficRules = 2,
        Social = 3,
        Animals = 4,
        Budget = 5,
        Other = 6

    }
}
