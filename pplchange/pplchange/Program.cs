﻿using Microsoft.EntityFrameworkCore;
using pplchange.Enums;
using pplchange.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace pplchange
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            Console.WriteLine("Welcome");
            MenuNav(loginMenu);
        }

        /// <summary>
        /// Основные переменные
        /// </summary>
        public static bool check;
        public static User currentUser;
        public static int i;
        public static string border = "  ----------------------------\n";
        public static string defaultResponse = $"  [Up] and [Down] to navigate."
                                              + "\n  [Enter] to choose options."
                                              + "\n  [Escape] to return.\n  ";
        public static string response = defaultResponse;
        public static Post currentPost;
        public static List<Option> currentMenuOption;


        /// <summary>
        /// Список опций в меню авторизации
        /// </summary>
        public static List<Option> loginMenu = new List<Option>
        {
            new Option("Login", () => Login()),
            new Option("Registration", () => MenuNav(ChoiceHigherEducation))
        };


        /// <summary>
        /// Список основного меню для UserPlus
        /// </summary>
        public static List<Option> userPlusMainMenu = new List<Option>
        {
            new Option("All posts", () => PrintPostList()),
            new Option("My posts", () => MyPosts()),
            new Option("Get Rating", () => GetRating()),
            new Option("Search", () => MenuNav(search)),
            new Option("Voting", () => Voting()),
            new Option("Add post", () => MenuNav(categoriesAddPost)),
            new Option("Logout", () => LogOut())
        };
        /// <summary>
        /// Список основного меню для User
        /// </summary>
        public static List<Option> userMainMenu = new List<Option>
        {
            new Option("All posts", () => PrintPostList()),
            new Option("Get Rating", () => GetRating()),
            new Option("Search", () => MenuNav(search)),
            new Option("Voting", () => Voting()),
            new Option("Logout", () => LogOut())
        };
        /// <summary>
        /// Поиск
        /// </summary>
        public static List<Option> search = new List<Option>
        {
            new Option("Search by category", () => MenuNav(categories)),
            new Option("Search by first or last name", () => NameSearchMethod())
        };

        /// <summary>
        /// Список категорий
        /// </summary>
        public static List<Option> categories = new List<Option>
        {
            new Option("Education", () => CallingPostsByCategory(categories)),
            new Option("TrafficRules", () => CallingPostsByCategory(categories)),
            new Option("Social", () => CallingPostsByCategory(categories)),
            new Option("Animals", () => CallingPostsByCategory(categories)),
            new Option("Budget", () => CallingPostsByCategory(categories)),
            new Option("Other", () => CallingPostsByCategory(categories))
        };

        /// <summary>
        /// Список категорий для добавления поста
        /// </summary>
        public static List<Option> categoriesAddPost = new List<Option>
        {
            new Option("Education", () => AddPost(categoriesAddPost)),
            new Option("TrafficRules", () => AddPost(categoriesAddPost)),
            new Option("Social", () => AddPost(categoriesAddPost)),
            new Option("Animals", () => AddPost(categoriesAddPost)),
            new Option("Budget", () => AddPost(categoriesAddPost)),
            new Option("Other", () => AddPost(categoriesAddPost))
        };

        /// <summary>
        /// список голосования
        /// </summary>
        public static List<Option> votingList = new List<Option>
        {
            new Option("YES", () => Vote()),
            new Option("NO", () => NotVote())
        };

        /// <summary>
        /// да нет для высшего образования
        /// </summary>
        public static List<Option> ChoiceHigherEducation = new List<Option>
        {
            new Option("YES", () => RegistrationUserPlus()),
            new Option("NO", () => RegistrationUser())
        };

        /// <summary>
        /// Метод входа в аккант пользователя.
        /// </summary>
        public static void Login()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var users = db.Users;

                Console.Clear();
                Console.Write("  Enter your login:\n  > ");
                var login = Console.ReadLine().ToLower();
                Console.Write("  Enter password:\n  > ");
                var password = Console.ReadLine();
                Console.WriteLine("Logging in...");

                var user = users.FirstOrDefault(x => x.Login == login && x.Password == password);
                if (user != null)
                {
                    currentUser = user;
                    Console.Clear();
                    Console.WriteLine($"Welcome {currentUser.Name}");
                    if(user.UserType == UserType.UserPlus)
                    {
                        currentMenuOption = userPlusMainMenu;
                    }
                    else if (user.UserType == UserType.User)
                    {
                        currentMenuOption = userMainMenu;
                    }
                    MenuNav(currentMenuOption);
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine($"  User {login} is not found or the password is incorrect! Press any key, to try again.");
                    Console.ReadKey();
                    Login();
                }
            }
        }

        /// <summary>
        /// Метод выхода из аккаунта пользователя.
        /// </summary
        public static void LogOut()
        {
            Console.Clear();
            MenuNav(loginMenu);
            currentMenuOption = null;
            currentUser = null;
        }
        /// <summary>
        /// Регистрация UserPlus
        /// </summary>
         public static void RegistrationUserPlus()
         {
             using (ApplicationDbContext db = new ApplicationDbContext())
             {
                 var users = db.Users;
                 var usersPlus = db.UsersPlus;
                Console.Clear();
                if (check)
                {
                    Console.Write($"  This login is already taken!\n  Enter different nickname.\n  > ");
                    check = false;
                }
                else
                {
                    Console.Write($"  Enter new login.\n  > ");
                }
                string login = Console.ReadLine().ToLower();
                var user = usersPlus.FirstOrDefault(x => x.Login == login);
                if (user != null)
                {
                    check = true;
                    RegistrationUserPlus();
                }
                Console.Write($"  Enter new password.\n  > ");
                string password = Console.ReadLine();
                if (password.Length < 8 || password.Length > 20)
                {
                    Console.WriteLine("Password length shold be more than 8 and less than 20. Press any key to try again.");
                    Console.ReadKey();
                    RegistrationUserPlus();
                }
                Console.Write($"  Enter your name.\n  > ");
                string name = Console.ReadLine();
                if (name.Length < 5 || name.Length > 20)
                {
                    RegistrationUserPlus();
                }
                Console.Write($"  Enter your surname.\n  > ");
                string surname = Console.ReadLine();
                if (surname.Length < 5 || surname.Length > 20)
                {
                    RegistrationUserPlus();
                }
                Console.Write($"  Enter your sex(M/F). \n > ");
                string sex = Console.ReadLine().ToLower();
                int sexInt = 0;
                if (sex == "m")
                {
                    sexInt = 2;
                }
                else if (sex == "f")
                {
                    sexInt = 1;
                }
                else
                {
                    Console.WriteLine("Wrong format! Press any key to try again");
                    Console.ReadLine();
                    RegistrationUserPlus();

                }
                Console.WriteLine($"  Enter your date of birth in format DD/MM/YYYY.");
                string dateOfBirthStr = Console.ReadLine();
                DateTime dateOfBirth;
                DateTime.TryParse(dateOfBirthStr, out dateOfBirth);
                Console.Write($"  Enter your university name.\n  > ");
                string university = Console.ReadLine();
                Console.Write($"  Enter your university contacts.\n  > ");
                string universityContacts = Console.ReadLine();
                usersPlus.Add(new UserPlus
                {
                    Name = name,
                    Surname = surname,
                    Login = login,
                    Password = password,
                    DateOfBirth = dateOfBirth,
                    CreatedAt = DateTime.Now,
                    University = university,
                    UniversityContacts = universityContacts,
                    UserType = UserType.UserPlus,
                    Sex = (Sex)sexInt
                });
                db.SaveChanges();
                Console.WriteLine($"  Registration has been succesfully completed  ");
                currentMenuOption = userPlusMainMenu;
                MenuNav(currentMenuOption);
            }
         }
        /// <summary>
        /// Регистрация Uses
        /// </summary>
        public static void RegistrationUser()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var users = db.Users;
                var usersPlus = db.UsersPlus;
                Console.Clear();
                if (check)
                {
                    Console.Write($"  This login is already taken!\n  Enter different login.\n  > ");
                    check = false;
                }
                else
                {
                    Console.Write($"  Enter new login.\n  > ");
                }

                string login = Console.ReadLine().ToLower();
                var user = users.FirstOrDefault(x => x.Login == login);
                if (user != null)
                {
                    check = true;
                    RegistrationUser();
                }

                Console.Write($"  Enter new password.\n  > ");
                string password = Console.ReadLine();
                if (password.Length < 8 || password.Length > 20)
                {
                    Console.WriteLine("Password length shold be more than 8 and less than 20. Press any key to try again.");
                    Console.ReadKey();
                    RegistrationUser();
                }
                Console.Write($"  Enter your name.\n  > ");
                string name = Console.ReadLine();
                if (name.Length < 5 || name.Length > 20)
                {
                    RegistrationUser();
                }
                Console.Write($"  Enter your surname.\n  > ");
                string surname = Console.ReadLine();
                if (surname.Length < 5 || surname.Length > 20)
                {
                    RegistrationUser();
                }
                Console.Write($"  Enter your sex(M/F). \n > ");
                string sex = Console.ReadLine().ToLower();
                int sexInt = 0;
                if (sex == "m")
                {
                    sexInt = 2;
                }
                else if (sex == "f")
                {
                    sexInt = 1;
                }
                else
                {
                    Console.WriteLine("Wrong format! Press any key to try again.");
                    Console.ReadKey();
                    RegistrationUser();
                }
                Console.WriteLine($"  Enter your date of birth in format DD/MM/YYYY.\n > ");
                string dateOfBirthStr = Console.ReadLine();
                DateTime dateOfBirth;
                DateTime.TryParse(dateOfBirthStr, out dateOfBirth);


                users.Add(new User
                {
                    Name = name,
                    Surname = surname,
                    Login = login,
                    Password = password,
                    DateOfBirth = dateOfBirth,
                    CreatedAt = DateTime.Now,
                    UserType = UserType.User,
                    Sex = (Sex)sexInt
                });

                db.SaveChanges();
                Console.WriteLine($"  Registration has been succesfully completed  ");
                currentMenuOption = userMainMenu;
                MenuNav(currentMenuOption);
            }
        }

        /// <summary>
        /// Метод добавления поста
        /// </summary>
        /// <param name="options"></param>
        public static void AddPost(List<Option> options)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                Console.Clear();
                var posts = db.Posts;

                int answer = i + 1;
                Category category = (Category)answer;
                Console.WriteLine($"You have selected this category: {category}");
                Console.WriteLine("Enter your name post:");
                string name = Console.ReadLine();
                if (name.Trim() == "")
                {
                    Console.WriteLine("Try again!");
                    AddPost(options);
                }
                Console.WriteLine("Enter your comment post:");
                string comment = Console.ReadLine();
                if (comment.Trim() == "")
                {
                    Console.WriteLine("Try again!");
                    AddPost(options);
                }


                posts.Add(new Post
                {
                    Category = category,
                    Name = name,
                    Comment = comment,
                    UserPlusID = currentUser.ID
                });
                db.SaveChanges();

                Program.LimitationSymbolPost(comment);
                MenuNav(categoriesAddPost);
            }
        }

        /// <summary>
        /// Ограничения по колличеству символов для поста
        /// </summary>
        /// <param name="comment"></param>
        public static void LimitationSymbolPost(string comment)
        {
            Console.Clear();
            if (comment.Length <= 4 || comment.Length >= 1024)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The message must be no more than 4 characters or less than 1024!");
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Метод вызова меню. Выводит заданный список опций List<Option> в консоль.
        /// </summary>
        /// <param name="options"></param>
        public static void Menu(List<Option> options)
        {
            if (options == votingList)
            {
                Console.Clear();
                SetDefaultColor();
                Header();
                PostPrintInfo(currentPost);
            }
            else
            {
                Console.Clear();
                SetDefaultColor();
                Header();
            }
            if (options == ChoiceHigherEducation)
            {
                HigherEducationChoiceHeader();
            }
            foreach (Option option in options)
            {
                if (option == options[i])
                {
                    Console.Write("  ");
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                else
                {
                    SetDefaultColor();
                    Console.Write("  ");
                }

                if (options == currentMenuOption)
                {
                    Console.WriteLine($"{AllignRight(option)}");
                }

                else
                {
                    Console.WriteLine($"{AllignRight(option)} ");
                }
            }

            SetDefaultColor();
            Console.Write(border);
            if (response != defaultResponse)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            Console.Write(response);
            response = defaultResponse;
        }

        /// <summary>
        /// Метод, отвечающий за навигацию в меню.
        /// </summary>
        /// <param name="options"></param>
        public static void Nav(List<Option> options)
        {
            ConsoleKeyInfo keyInfo;
            do
            {
                keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.DownArrow && i + 1 < options.Count)
                {
                    i++;
                    Menu(options);
                }
                if (keyInfo.Key == ConsoleKey.UpArrow && i - 1 >= 0)
                {
                    i--;
                    Menu(options);
                }
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    options[i].Selected.Invoke();
                }
                if (keyInfo.Key == ConsoleKey.Escape)
                {
                    if (currentUser != null)
                    {
                        i = 0;
                        Menu(currentMenuOption);
                        break;
                    }
                }
            }
            while (keyInfo.Key != ConsoleKey.X);
        }

        /// <summary>
        /// Общий метод вызова меню и навигации.
        /// </summary>
        /// <param name="options"></param>

        public static void MenuNav(List<Option> options)
        {
            i = 0;
            if (options == categoriesAddPost && currentUser.UserType != UserType.UserPlus)
            {
                response = "Only UsersPlus are allowed to add new posts!";
                MenuNav(currentMenuOption);
                return;
            }
            Menu(options);
            Nav(options);
        }

        /// <summary>
        /// Метод вывода свех постов
        /// </summary>
        public static void PrintPostList()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var posts = db.Posts.Include(x => x.Votes);
                Console.Clear();
                PostListHeader();
                int index = 1;
                foreach (Post post in posts)
                {
                    Console.Write(index);
                    PostPrintInfo(post);
                    index++;
                }

                Console.WriteLine(border);
            }
        }

        /// <summary>
        /// Mетод вызова постов по категориям
        /// </summary>
        public static void CallingPostsByCategory(List<Option> options)
        {
            using (var db = new ApplicationDbContext())
            {
                var posts = db.Posts.Include(x => x.Votes);
                int answer = i + 1;
                Console.Clear();
                foreach (Post post in posts)
                {
                    if (post.Category == (Category)answer)
                    {
                        PostPrintInfo(post);
                    }
                }
            }
        }

        /// <summary>
        /// Mетод голосования
        /// </summary>
        public static void Vote()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                db.Votes.Add(new Vote()
                {
                    PostID = currentPost.ID,
                    UserID = currentUser.ID,
                    Choice = true
                });
                Console.WriteLine("You have successfully voted");
                db.SaveChanges();
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                MenuNav(currentMenuOption);
                currentPost = null;
            }
        }
        /// <summary>
        /// Mетод голосования (против)
        /// </summary>
        public static void NotVote()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                    db.Votes.Add(new Vote()
                    {
                        PostID = currentPost.ID,
                        UserID = currentUser.ID,
                        Choice = false
                    });
                    Console.WriteLine("You have successfully voted");
                    db.SaveChanges();
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                    MenuNav(currentMenuOption);
                    currentPost = null;
            }
        }

        /// <summary>
        /// Метод вывода "шапки" страницы.
        /// </summary>
        public static void Header()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var posts = db.Posts;
                int p = 0;
                var userPosts = posts.Where(x => x.UserPlusID == currentUser.ID);
                if (currentUser != null)
                {
                    p = userPosts.Count();
                }
                DateTime dateTime = DateTime.Now;
                string date = dateTime.ToShortDateString();
                if (currentUser == null)
                {
                    Console.WriteLine("   Welcome to Freedom of people's voices");
                }
                    if (currentUser != null)
                {
                    Console.WriteLine($"  {date}");
                    Console.WriteLine($"  {currentUser.Name} {currentUser.Surname}");
                    Console.WriteLine($"  Number of posts - {p}");
                    Console.WriteLine($"User Type: {currentUser.UserType}");
                }
                Console.Write(border);
            }
        }

        /// <summary>
        /// Заголовок Выбора Высшего Оразования
        /// </summary>
        public static void HigherEducationChoiceHeader()
        {
            Console.WriteLine("Do you have Bachelor degree?");
        }

        /// <summary>
        /// Цвет по умолчанию
        /// </summary>
        public static void SetDefaultColor()
        {
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;
        }

        /// <summary>
        /// Выравнивает заданную опцию по правому краю.
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public static string AllignRight(Option option)
        {
            string rightText = "";
            while (22 - rightText.Length - option.Name.Length > 0)
            {
                rightText += " ";
            }
            rightText += option.Name;
            return rightText;
        }

        /// <summary>
        /// Поиск по имени
        /// </summary>
        public static void NameSearchMethod()
        {
            Console.Clear();
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var posts = db.Posts.Include(x => x.Votes);
                var userpluses = db.UsersPlus.ToList();
                Console.WriteLine("Enter the UserPlus name or surname");
                var name = Console.ReadLine();
                foreach (UserPlus userPlus in userpluses.Where(n => n.Name.ToLower().Contains(name.ToLower()) || n.Surname.ToLower().Contains(name.ToLower())))
                {
                    foreach (Post post in posts.Where(n => n.UserPlusID == userPlus.ID))
                    {
                        PostPrintInfo(post);
                    }
                }
            }
        }

        /// <summary>
        /// Система голосования
        /// </summary>
        public static void Voting()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                Console.Clear();
                Header();
                Console.WriteLine(border);
                Console.WriteLine("  Voting menu by id");
                Console.Write("  Enter post's ID: ");

                int id = 0;
                bool answer = false;
                if (!answer)
                {
                    answer = int.TryParse(Console.ReadLine(), out id);
                    Console.WriteLine("wrong format");
                    if (!answer && id < 0)
                    {
                        Console.WriteLine("Invalid input or negative number");
                    }
                }

                var posts = db.Posts.Include(x => x.Votes);
                var post = posts.FirstOrDefault(x => x.ID == id);
                var votes = db.Votes;
                if (post != null)
                {
                    var votess = post.Votes.FirstOrDefault(x => x.UserID == currentUser.ID);
                    var postss = post.UserPlusID == currentUser.ID;


                    if (votess == null && !postss)
                    {
                            currentPost = post;
                            MenuNav(votingList);                    
                    }
                    else if (votess != null)
                    {
                        Console.WriteLine("Sorry, you can't vote for the post because you already voted for this post.");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        MenuNav(currentMenuOption);
                    }
                    else
                    {
                        Console.WriteLine("Sorry, you can't vote for YOUR post.");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadKey();
                        MenuNav(currentMenuOption);
                    }
                }
            }
        }

        /// <summary>
        /// Информация о посте
        /// </summary>
        /// <param name="post"></param>
        public static void PostPrintInfo(Post post)
        {
            if(post != null)
            {
                Console.WriteLine($"  Id {post.ID} \n   Category {post.Category} \n   Name {post.Name} \n   Comment {post.Comment}");
                var overallPositive = post.Votes.Where(x => x.Choice == true).Count();
                var overallNegative = post.Votes.Where(x => x.Choice == false).Count();
                Console.WriteLine($"  Positive votes: {overallPositive}");
                Console.WriteLine($"  Negative votes: {overallNegative} \n");
                Console.Write(border);
            }
            else
            {
                Console.WriteLine("There is no post with such ID");
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
                MenuNav(currentMenuOption);
            }
        }

        /// <summary>
        /// Заголовок для листа постов
        /// </summary>
        public static void PostListHeader()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var posts = db.Posts;
                int p = 0;
                var userPosts = posts.Where(x => x.UserPlusID == currentUser.ID);
                if (currentUser != null)
                {
                    p = userPosts.Count();
                }
                Console.WriteLine("       All posts menu");
                Console.Write(border);
                Console.WriteLine($"     Number of posts : {p}");
            }
        }
        /// <summary>
        /// метод вывода рейтинга поста
        /// </summary>
        public static void GetRating()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var posts = db.Posts.Include(x => x.Votes).OrderByDescending(x => x.Votes.Where(x => x.Choice == true).Count());
                Console.Clear();
                if (posts != null)
                {
                    foreach (Post post in posts)
                    {
                        PostPrintInfo(post);
                    }
                }
            }
        }

        public static void MyPosts()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var posts = db.Posts.Include(x => x.Votes).Where(x => x.UserPlusID == currentUser.ID).ToList();
                if (posts != null)
                {
                    foreach (Post post in posts)
                    {
                        PostPrintInfo(post);
                    }
                }
                else
                {
                    Console.WriteLine("You don't have posts");
                }
            }
        }

        static int Prowerka(string message)
        {
            int number = 0;
            bool answer = false;
            while (!answer)
            {
                Console.WriteLine(message);
                answer = int.TryParse(Console.ReadLine(), out number);
                if (!answer && number < 0)
                {
                    Console.WriteLine("Invalid input or negative number");
                }
            }
            return number;
        }
    }
}
