﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using pplchange.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace pplchange
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserPlus> UsersPlus { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Vote> Votes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Users\Admin\Desktop\ItAcademy\GitRepositories\pplchange\pplchange\pplchange");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();

            var connectionString = config.GetConnectionString("DefaultConnection");

            optionsBuilder.UseSqlServer(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Post>()
                .HasIndex(x => x.Name)
                .IsUnique();
            modelBuilder
                .Entity<User>()
                .HasIndex(x => x.Login)
                .IsUnique();
        }
    }
}
