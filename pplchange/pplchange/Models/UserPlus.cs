﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pplchange.Models
{
    public class UserPlus : User
    {
        public string University { get; set; }
        public string UniversityContacts { get; set; }
    }
}
