﻿using pplchange.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace pplchange.Models
{
    public class Option
    {
        
        public Option(string name, Action selected)
        {
            Name = name;
            Selected = selected;
        }
        public string Name { get; }
        public Action Selected { get; }
    }
}
