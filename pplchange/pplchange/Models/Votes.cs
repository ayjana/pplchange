﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace pplchange.Models
{
    public class Vote : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("PostID")]
        public int PostID { get; set; }

        [ForeignKey("UserID")]
        public int UserID { get; set; }
        public bool Choice { get; set; }
        public virtual User User { get; set; }
        public virtual Post Post { get; set; }
    }
}
