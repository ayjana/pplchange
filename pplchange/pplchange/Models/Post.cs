﻿using pplchange.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace pplchange.Models
{
    public class Post : IEntity<int>
    {
        public int ID { get; set; }
        public Category Category { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public List<Vote> Votes { get; set; }

        [ForeignKey("UserPlusID")]
        public int UserPlusID { get; set; }
        public virtual UserPlus UserPlus { get; set; }
    }
}
